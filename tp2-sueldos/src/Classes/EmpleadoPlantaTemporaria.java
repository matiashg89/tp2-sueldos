package Classes;

import java.io.IOException;
import java.util.Date;
import java.util.ArrayList;

import Util.Calculadora;
import Util.FileManager;

public class EmpleadoPlantaTemporaria extends Empleado{	
	private Date fechaDesignacion;
	private double cantidadHorasExtra;
	
	//Constructor
	public EmpleadoPlantaTemporaria(String nombre, String direccion, Boolean estaCasado, Date fechaNacimiento, double sueldoBasico, Date fechaDesignacion, double cantidadHorasExtra,FileManager fm, Calculadora calcu) {
		super(nombre,direccion,estaCasado,fechaNacimiento,sueldoBasico,fm,calcu);
		this.setFechaDesignacion(fechaDesignacion);
		this.setCantidadHorasExtra(cantidadHorasExtra);
	}

	//Getters y Setters
	public Date getFechaDesignacion() {
		return fechaDesignacion;
	}
	public void setFechaDesignacion(Date fechaDesignacion) {
		this.fechaDesignacion = fechaDesignacion;
	}
	public double getCantidadHorasExtra() {
		return cantidadHorasExtra;
	}
	public void setCantidadHorasExtra(double cantidadHorasExtra) {
		this.cantidadHorasExtra = cantidadHorasExtra;
	}	
	
	//Definicion de metodos abstractos
	public double calcularSueldoBruto() {
		return this.sueldoBasico + this.cantidadHorasExtra * 40;
	}
	public double calcularRetenciones() {
		return this.calcularRetencionSueldoBrutoYAntiguedad() + this.calcularRetencionHorasExtra() + this.calcularAportesJubilatorios();
	}
	
	public void liquidarSueldo() throws IOException {
		this.fileManager.WriteFile(this.FILE_PATH + "/" + this.nombre + ".txt", this.obtenerCuerpoLiquidacion());
	}
	
	//Metodos de instancia	
	public ArrayList<String> obtenerCuerpoLiquidacion(){
		ArrayList<String> cuerpo = new ArrayList<String>();
		
		cuerpo.add("----------------------------------------------------------");
		cuerpo.add("-------------------- RECIBO DE SUELDO --------------------");		
		cuerpo.add("----------------------------------------------------------");
		cuerpo.add("Nombre: " + this.nombre);
		cuerpo.add("Direccion: " + this.direccion);
		cuerpo.add("Fecha emisi�n: ");
		cuerpo.add("Sueldo Bruto: " + this.calcularSueldoBruto());
		cuerpo.add("Sueldo Neto: " + this.calcularSueldoNeto());
		cuerpo.add("Retencion Sueldo Bruto: " + this.calcularRetencionesSueldoBruto());
		cuerpo.add("Retencion por antiguedad: " + this.calcularRetencionesAntiguedad());
		cuerpo.add("Retencion horas extra: " + this.calcularRetencionHorasExtra());
		cuerpo.add("Aportes jubilatorios: " + this.calcularAportesJubilatorios());
		cuerpo.add("----------------------------------------------------------");
		return cuerpo;
	}
	
	private double calcularRetencionSueldoBrutoYAntiguedad() {
		return this.calcularRetencionesSueldoBruto() + this.calcularRetencionesAntiguedad();
	}
	
	private double calcularRetencionesSueldoBruto() {
		return this.calcularSueldoBruto() * 0.1;
	}
	
	private double calcularRetencionesAntiguedad() {
		return this.calculadora.calcularAntiguedad(this.fechaNacimiento) > 50 ? 25 : 0;
	}
	
	private double calcularRetencionHorasExtra() {
		return this.cantidadHorasExtra * 5;
	}
	
	private double calcularAportesJubilatorios() {
		return this.calcularSueldoBruto() * 0.1;
	}
}
