package Classes;
import Util.Calculadora;

import java.io.IOException;
import java.util.Date;
import java.util.ArrayList;

import Util.FileManager;

public class EmpleadoPlantaPermanente extends Empleado {
	private double cantidadHijos;
	private Date fechaIngreso;	
	
	//Constructor
	public EmpleadoPlantaPermanente(String nombre, String direccion, Boolean estaCasado, Date fechaNacimiento, double sueldoBasico, double cantidadHijos, Date fechaIngreso,FileManager fm, Calculadora calcu) {
		super(nombre,direccion,estaCasado,fechaNacimiento,sueldoBasico,fm,calcu);
		this.cantidadHijos = cantidadHijos;
		this.fechaIngreso = fechaIngreso;
	}
	
	//Getters y Setters
	public double getCantidadHijos() {
		return cantidadHijos;
	}
	public void setCantidadHijos(double cantidadHijos) {
		this.cantidadHijos = cantidadHijos;
	}
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	
	//Definicion de metodos abstractos
	public double calcularSueldoBruto() {
		return this.sueldoBasico +  this.calcularAsignacionPorHijo() + this.calcularAsignacionPorConyuge() + this.calcularAsignacionPorAntiguedad();
	}
	public double calcularRetenciones() {
		return this.calcularRetencionSueldoBruto() + this.calcularRetencionPorHijos() + this.calcularAportesJubilatorios();
	}
	
	public void liquidarSueldo() throws IOException {
		this.fileManager.WriteFile(this.FILE_PATH + "/" + this.nombre + ".txt", this.obtenerCuerpoLiquidacion());
	}
	
	//Metodos de instancia
	private ArrayList<String> obtenerCuerpoLiquidacion(){
		ArrayList<String> cuerpo = new ArrayList<String>();
		
		cuerpo.add("---------------------------------------------------------");
		cuerpo.add("Nombre: " + this.nombre);
		cuerpo.add("Direccion: " + this.direccion);
		cuerpo.add("Fecha emisi�n: ");
		cuerpo.add("Sueldo Bruto: " + this.calcularSueldoBruto());
		cuerpo.add("Sueldo Neto: " + this.calcularSueldoNeto());
		cuerpo.add("Asignacion por Hijo: " + this.calcularAsignacionPorHijo());
		cuerpo.add("Asignacion por Conyuge: " + this.calcularAsignacionPorConyuge());
		cuerpo.add("Retencion Sueldo Bruto: " + this.calcularRetencionSueldoBruto());
		cuerpo.add("Retencion por Hijos: " + this.calcularRetencionPorHijos());
		cuerpo.add("Aportes jubilatorios: " + this.calcularAportesJubilatorios());
		cuerpo.add("---------------------------------------------------------");
		return cuerpo;
	}
	
	private double calcularAsignacionPorHijo() {
		return 150 * this.cantidadHijos;
	}	
	private double calcularAsignacionPorConyuge() {
		return this.estaCasado ? 100 : 0;
	}
	private double calcularAsignacionPorAntiguedad() {
		return 50 * this.calculadora.calcularAntiguedad(this.fechaIngreso);
	}
	
	private double calcularRetencionSueldoBruto() {
		return this.calcularSueldoBruto() * 0.1;
	}
	
	private double calcularRetencionPorHijos() {
		return this.cantidadHijos * 20;
	}
	
	private double calcularAportesJubilatorios() {
		return this.calcularSueldoBruto() * 0.15;
	}
	
	
	
}
