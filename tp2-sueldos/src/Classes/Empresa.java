package Classes;

import java.io.IOException;
import java.util.ArrayList;

public class Empresa {
	private String nombre;
	private String cuit;
	private ArrayList<Empleado> empleados; 
	
	//Constructor
	public Empresa(String nombre, String cuit) {
		this.nombre = nombre;
		this.cuit = cuit;
		this.empleados = new ArrayList<Empleado>();
	}

	//Getters y Setters
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCuit() {
		return cuit;
	}
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	public ArrayList<Empleado> getEmpleados() {
		return empleados;
	}
	public void nuevoEmpleado(Empleado empleado) {
		this.empleados.add(empleado);
	}
	
	public double totalSueldosNetos() {
		return this.empleados.stream().mapToDouble(empleado -> empleado.calcularSueldoNeto()).sum();
	}
	
	public double totalSueldosBruto() {
		return this.empleados.stream().mapToDouble(empleado -> empleado.calcularSueldoBruto()).sum();
	}
	
	public double totalRetenciones() {
		return this.empleados.stream().mapToDouble(empleado -> empleado.calcularRetenciones()).sum();
	}
	
	public void liquidarSueldos() {
		this.empleados.forEach(empleado -> {
			try {
				empleado.liquidarSueldo();
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}
	
	
	
	
	
}
