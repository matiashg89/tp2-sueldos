package Classes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import Util.Calculadora;
import Util.FileManager;

public class EmpleadoContratado extends Empleado{
	private double numeroContrato;
	private String medioPago;
	
	//Constructor
	public EmpleadoContratado(String nombre, String direccion, Boolean estaCasado, Date fechaNacimiento, double sueldoBasico, double numeroContrato, String medioPago,FileManager fm, Calculadora calcu) {
		super(nombre,direccion,estaCasado,fechaNacimiento,sueldoBasico,fm, calcu);
		this.setNumeroContrato(numeroContrato);
		this.setMedioPago(medioPago);		
	}

	//Getters y Setters
	public double getNumeroContrato() {
		return numeroContrato;
	}
	public void setNumeroContrato(double numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	public String getMedioPago() {
		return medioPago;
	}
	public void setMedioPago(String medioPago) {
		this.medioPago = medioPago;
	}
	//Definicion de metodos abstractos
	public double calcularSueldoBruto() {
		return this.sueldoBasico;
	}
	public double calcularRetenciones() {
		return 50;
	}
	
	public void liquidarSueldo() throws IOException {
		this.fileManager.WriteFile(this.FILE_PATH + "/" + this.nombre + ".txt", this.obtenerCuerpoLiquidacion());
	}
	
	//Metodos de instancia	
	public ArrayList<String> obtenerCuerpoLiquidacion(){
		ArrayList<String> cuerpo = new ArrayList<String>();		
		cuerpo.add("----------------------------------------------------------");
		cuerpo.add("-------------------- RECIBO DE SUELDO --------------------");		
		cuerpo.add("----------------------------------------------------------");
		cuerpo.add("Nombre: " + this.nombre);
		cuerpo.add("Direccion: " + this.direccion);
		cuerpo.add("Fecha emisi�n: ");
		cuerpo.add("Sueldo Bruto: " + this.calcularSueldoBruto());
		cuerpo.add("Sueldo Neto: " + this.calcularSueldoNeto());
		cuerpo.add("Gastos Administrativos Contractuales: " + this.calcularRetenciones());
		cuerpo.add("----------------------------------------------------------");
		return cuerpo;
	}
}
