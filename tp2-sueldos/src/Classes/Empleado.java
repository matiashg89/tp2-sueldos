package Classes;

import java.io.IOException;
import java.util.Date;

import Util.Calculadora;
import Util.FileManager;

public abstract class Empleado {
	final String FILE_PATH = "../TP2-Sueldos/liquidaciones";
	protected String nombre;
	protected String direccion;
	protected Boolean estaCasado;
	protected Date fechaNacimiento;
	protected double sueldoBasico;
	protected FileManager fileManager;
	protected Calculadora calculadora;
	
	//Constructor
	public Empleado(String nombre, String direccion, Boolean estaCasado, Date fechaNacimiento, double sueldoBasico, FileManager fm, Calculadora calcu) {
		this.nombre = nombre;
		this.direccion = direccion;
		this.estaCasado = estaCasado;
		this.fechaNacimiento = fechaNacimiento;
		this.sueldoBasico = sueldoBasico;
		this.fileManager = fm;
		this.calculadora = calcu;
	}
	
	//Getters y Setters
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public Boolean getEstaCasado() {
		return estaCasado;
	}
	public void setEstaCasado(Boolean estaCasado) {
		this.estaCasado = estaCasado;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public double getSueldoBasico() {
		return sueldoBasico;
	}
	public void setSueldoBasico(double sueldoBasico) {
		this.sueldoBasico = sueldoBasico;
	}
	
	//Metodos de instancia
	public double calcularSueldoNeto() {
		return this.calcularSueldoBruto() - this.calcularRetenciones();
	}
	//Metodos abstractos
	abstract double calcularSueldoBruto();
	abstract double calcularRetenciones();	
	abstract void liquidarSueldo() throws IOException;
}
