package Util;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class FileManager {

	public void WriteFile(String path,ArrayList<String> text) throws IOException {
		try {
            FileWriter writer = new FileWriter(path, false);

            for (int i = 0; i < text.size(); i++) {
            	writer.write(text.get(i));
                writer.write("\r\n");
			}
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
}
