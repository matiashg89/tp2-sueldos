package Main;
import java.util.Date;

import Classes.*;
import Util.Calculadora;
import Util.FileManager;

public class main {

	public static void main(String[] args) {
		EmpleadoPlantaPermanente empleado1 = new EmpleadoPlantaPermanente(
			"Matias Gonzalez", "Direccion_",false,new Date(1989,04,25),1000,0,new Date(2000,11,05),new FileManager(),new Calculadora()
		);
		
		EmpleadoPlantaPermanente empleado2 = new EmpleadoPlantaPermanente(
			"Pepe", "Direccion 2",true,new Date(),2000,2,new Date(),new FileManager(),new Calculadora()
		);
		
		EmpleadoPlantaPermanente empleado3 = new EmpleadoPlantaPermanente(
			"Maria", "Direccion 3",false,new Date(),10000,0,new Date(),new FileManager(),new Calculadora()
		);
		
		EmpleadoPlantaTemporaria empleado4 = new EmpleadoPlantaTemporaria(
			"Marcela", "Direccion 4",true,new Date(),5000,new Date(),7,new FileManager(),new Calculadora()
		);
		
		EmpleadoPlantaTemporaria empleado5 = new EmpleadoPlantaTemporaria(
			"Carlos", "Direccion 5",false,new Date(),7800,new Date(),2,new FileManager(),new Calculadora()
		);
		
		EmpleadoContratado empleado6 = new EmpleadoContratado(
			"Juan", "Direccion 5",false,new Date(),7800,1,"Cheque",new FileManager(),new Calculadora()
		);
		
		EmpleadoContratado empleado7 = new EmpleadoContratado(
			"Carla", "Direccion 5",false,new Date(),7800,2,"Efectivo",new FileManager(),new Calculadora()
		);
		
		Empresa miEmpresa = new Empresa("Empresa Matias","123123123");
		
		miEmpresa.nuevoEmpleado(empleado1);
		miEmpresa.nuevoEmpleado(empleado2);
		miEmpresa.nuevoEmpleado(empleado3);
		miEmpresa.nuevoEmpleado(empleado4);
		miEmpresa.nuevoEmpleado(empleado5);
		miEmpresa.nuevoEmpleado(empleado6);
		miEmpresa.nuevoEmpleado(empleado7);
		
		System.out.println("Total sueldos Neto: " + miEmpresa.totalSueldosNetos());
		System.out.println("Total sueldos Bruto: " + miEmpresa.totalSueldosBruto());
		System.out.println("Total Retenciones: " + miEmpresa.totalRetenciones());
		
		miEmpresa.liquidarSueldos();

	}

}
